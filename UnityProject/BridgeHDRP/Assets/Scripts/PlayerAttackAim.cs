﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackAim : MonoBehaviour
{
    Camera mainCamera;
    Ray camRay;

    public LayerMask AttackableMask;

    private Transform targetEnemy;
    private Transform[] targetEnemies;

    PlayerFocus playerfocus;

    // Start is called before the first frame update
    void Awake()
    {
        playerfocus = GetComponent<PlayerFocus>();

        mainCamera = Camera.main;
        camRay = mainCamera.ScreenPointToRay(Input.mousePosition);

        AttackableMask = LayerMask.GetMask("Attackable");
  
    }

    // Update is called once per frame
    void Update()
    {
        camRay = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            camRay = mainCamera.ScreenPointToRay(Input.mousePosition);RaycastHit camRaycasthit;

            if (Physics.Raycast(camRay, out camRaycasthit, 100.0f, AttackableMask))
            {
                /*
                // 目前問題是collider重疊時的優先順序--1
                Attackable enemy = camRaycasthit.collider.GetComponent<Attackable>();
                
                if (enemy != null)
                {
                    Debug.Log("Got a attackableRef " + enemy.name);

                }*/

                // 改寫成射到地面抓近的 // 後來發現這樣很不直觀--2
                /*
                GameObject[] allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
                //Debug.Log(SortEnemiesWithDistant(allEnemies, camRaycasthit.point));
                Debug.Log("幹你娘卡巴斯基");

                // 排序debug
                for (int i = 0; i < SortEnemiesWith2dDistant(allEnemies, camRaycasthit.point).Length; i++)
                {
                    Debug.Log(SortEnemiesWith2dDistant(allEnemies, camRaycasthit.point)[i].GetComponent<Attackable>().name);
                }*/

                // 如果改成由角色發射範圍來決定攻擊誰說不定更好 然後要分兩種 精準再來方向性的--3 // 可能可以用三種判定 精準按到collider > 點到地板算距離 > 方向性 
                // 又或者把敵人的位置抓到螢幕上  cam.WorldToScreenPoint(target.position);
            }

            // 把敵人位置抓到螢幕上
            GameObject[] allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < SortEnemiesWithScreenDistant(allEnemies, Input.mousePosition).Length; i++)
            {
                Debug.Log(SortEnemiesWithScreenDistant(allEnemies, Input.mousePosition)[i].GetComponent<Attackable>().name);
            } // 這邊進展還不錯 就差在點的差太遠的時候還是會抓清單 總不能點反方向還是讓他攻擊

            SetTargetEnemy(SortEnemiesWithScreenDistant(allEnemies, Input.mousePosition));

            // 按下左鍵的時候把focus設為按到的敵人
            playerfocus.OnFocus();
        }
    }



    // 把物件投射到螢幕的距離排序
    public Transform[] SortEnemiesWithScreenDistant(GameObject[] _allEnemies, Vector3 _mousePointOnScreen)
    {
        Transform[] sortedEnemies = new Transform[_allEnemies.Length];

        // 簡單的氣泡排序
        for (int i = 0; i < _allEnemies.Length - 1; i++)
        {
            GameObject temp;
            Camera mainCamera = Camera.main;

            // 先取得物品在螢幕上的位置再刪掉物件的z軸值(因為在螢幕上不須深度)
            Vector3 pointAinScreen = mainCamera.WorldToScreenPoint(_allEnemies[i].transform.position);
            pointAinScreen.z = 0;

            // 先取得物品在螢幕上的位置再刪掉物件的z軸值(因為在螢幕上不須深度)
            Vector3 pointBinScreen = mainCamera.WorldToScreenPoint(_allEnemies[i + 1].transform.position);
            pointBinScreen.z = 0;

            if (Vector3.Distance(pointAinScreen, _mousePointOnScreen) > Vector3.Distance(pointBinScreen, _mousePointOnScreen))
            {
                temp = _allEnemies[i + 1];
                _allEnemies[i + 1] = _allEnemies[i];
                _allEnemies[i] = temp;
            }
        }

        for (int i = 0; i < _allEnemies.Length; i++)
        {
            sortedEnemies[i] = _allEnemies[i].transform;
        }


        return sortedEnemies;
    }

    // 含有y值距離的物件排序
    public Transform[] SortEnemiesWith3dDistant(GameObject[] _allEnemies, Vector3 _origin)
    {
        Transform[] sortedEnemies = new Transform[_allEnemies.Length];

        // 簡單的氣泡排序
        for (int i = 0; i < _allEnemies.Length - 1; i++)
        {
            GameObject temp;
            if(Vector3.Distance(_allEnemies[i].transform.position, _origin) > Vector3.Distance(_allEnemies[i+1].transform.position, _origin))
            {
                temp = _allEnemies[i + 1];
                _allEnemies[i + 1] = _allEnemies[i];
                _allEnemies[i] = temp;
            }
        }

        for (int i = 0; i < _allEnemies.Length; i++)
        {
            sortedEnemies[i] = _allEnemies[i].transform;
        }
        

        return sortedEnemies;
    }

    //"不含有Y值距離的物件排序"
    public Transform[] SortEnemiesWith2dDistant(GameObject[] _allEnemies, Vector3 _origin)
    {
        Transform[] sortedEnemies = new Transform[_allEnemies.Length];

        // 簡單的氣泡排序
        for (int i = 0; i < _allEnemies.Length - 1; i++)
        {
            GameObject temp;
            // 先刪掉物件的Y軸值
            Vector3 pointAwithoutY = _allEnemies[i].transform.position;
            pointAwithoutY.y = 0;

            // 先刪掉物件的Y軸值
            Vector3 pointBwithoutY = _allEnemies[i + 1].transform.position;
            pointBwithoutY.y = 0;

            if (Vector3.Distance(pointAwithoutY,_origin) > Vector3.Distance(pointBwithoutY, _origin))
            {
                temp = _allEnemies[i + 1];
                _allEnemies[i + 1] = _allEnemies[i];
                _allEnemies[i] = temp;
            }
        }

        for (int i = 0; i < _allEnemies.Length; i++)
        {
            sortedEnemies[i] = _allEnemies[i].transform;
        }


        return sortedEnemies;

    }

    // 設定只要單一目標時的目標
    public void SetTargetEnemy(Transform[] _sortedEnemies)
    {
        targetEnemy = _sortedEnemies[0].transform;
    }

    // 設定多重目標
    public void SetTargetEnemies(Transform[] _sortedEnemies)
    {
        targetEnemies = _sortedEnemies;
    }

    // 給其他class拿最近單一敵人的資料
    public Transform GetTargetEnemy()
    {
        return targetEnemy;
    }

    // 給其他class拿最近多重敵人的資料
    public Transform[] GetTargetEnemies()
    {

        return targetEnemies;
    }

    // 這個可以在其他class手動取前n個 但傳入的陣列要先排好(還不知道要拿來幹嘛反正都寫了
    public Transform[] GetTargetEnemies(Transform[] _allEnemies, int amount)
    {
        Transform[] target = new Transform[amount];
        for (int i = 0; i < amount; i++)
        {
            target[i] = _allEnemies[i];
        }

        return target;
    }


}
