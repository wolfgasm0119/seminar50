﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public Transform interactionTransform;  // 可以利用一個空物件變更物件的可互動範圍的中心
    public string name;

    bool isFocus = false;
    Transform player;

    bool hasInteracted = false;

    public virtual void Interact()
    {
        Debug.Log("與" + transform + "互動中"); // 之後也許給個每個物件命名欄位
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFocus)
        {
            if (!hasInteracted)
            {
                // 先判斷玩家及此物件的interactionTransform的距離 小於radius(也就是在raius裡面)才能互動
                /*float distance = Vector3.Distance(player.position, interactionTransform.position);
                if (distance <= radius)
                {
                    Interact();
                    hasInteracted = true;
                }*/
            }
        }

    }

    public void OnFocused(Transform _playerTransform)
    {
        isFocus = true;
        player = _playerTransform;
        hasInteracted = false;
    }

    public void OnDefocused()
    {
        isFocus = false;
        player = null;
        hasInteracted = false;
    }

    // 在edit模式畫出選中物件的互動範圍
    private void OnDrawGizmosSelected()
    {
        // 如果沒有用另一個物件當作互動範圍中心 就用物件本身的位置當中心
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
