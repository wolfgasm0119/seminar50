﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerAni : MonoBehaviour
{
    Animator playerAnimator;
    PlayerMovement playermovement;
    void Awake()
    {
        playerAnimator = GetComponentInChildren<Animator>();
        playermovement = GetComponent<PlayerMovement>();
    }

    
    void Update()
    {
        playerAnimator.SetBool("IsRunning", playermovement.isRunning);


    }
}
