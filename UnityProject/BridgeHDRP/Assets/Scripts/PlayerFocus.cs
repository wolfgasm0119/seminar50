﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerAttackAim))]
public class PlayerFocus : MonoBehaviour
{
    public Transform focus;
    private PlayerMovement playermovement;
    private PlayerAttackAim playerAttackAim;
    private Animator animator;
    void Start()
    {
        playermovement = GetComponent<PlayerMovement>();
        playerAttackAim = GetComponent<PlayerAttackAim>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       // if(Input.GetKeyDown(KeyCode.Mouse0))
        //{ 

            // 目前getargetenemy的值不太會變動 這樣可能造成玩家亂點還是會抓到敵人的情況 之後要改playerattackaim裡的方法 點歪就要清空
            // 然後現在邊移動邊按下左鍵抓敵人會沒效果 得要停下來在抓才有效 之後也要改
            // 然後要怎麼讓角色往怪身上跑= = 跑到radius要停下來()一開始就算好應該跑的距離 還是即時更新?) 寫完要處理一下下面的問題
            //focus = playerAttackAim.GetTargetEnemy();
        //}

        if (focus != null) // 應該要改成抓到東西後也不一定要做事情
        {
            if (focus.GetComponent<Attackable>() != null)
            {
                playermovement.FaceTo(focus.position, transform.position, true);
                //playermovement.MoveForward();

                //playermovement.MoveToward(focus.position, 15, focus.GetComponent<Attackable>().radius); // 這裡用delegate  = null 讓他停止跑動好像很適合
                // 現在問題是邊跑邊鎖敵人會爆開
                playermovement.playerDashEvent.Invoke(focus.position, 15, focus.GetComponent<Attackable>().radius);
            }
        }

        //Debug.Log(focus.GetComponent<Attackable>().name);
    }

    public void OnFocus()
    {
        focus = playerAttackAim.GetTargetEnemy();
        //animator.SetBool("IsDashing", true);

        playermovement.playerDashEvent += playermovement.MoveToward;
        playermovement.playerDashEvent -= new PlayerMovement.PlayerDashEvent(playermovement.MoveTowardStop);
    }

    public void Defocus()
    {
        focus = null;
        animator.SetBool("IsDashing", false);
    }
}
