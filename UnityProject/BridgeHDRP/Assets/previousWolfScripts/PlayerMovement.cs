﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerFocus))]
public class PlayerMovement : MonoBehaviour
{
    // 角色移動速度
    public float maxSpeed = 10.0f;
    public float currentSpeed = 0;
    public float acceleration = 6.5f;
    

    Vector3 movement;
    Animator playerAnimator;
    Rigidbody playerRigidbody;

    // 射線要偵測的Layer
    public LayerMask floorMask;
    public LayerMask playerMask;
    float camRayLength = 100f;  // 射線長度

    // 射線定義
    Ray camRay;

    // 傳給playerani控制走路動畫
    public bool isRunning;

    private PlayerFocus playerFocus;

    public delegate void PlayerDashEvent(Vector3 _targetPos, float _speed, float stopDistance);
    public PlayerDashEvent playerDashEvent;
    void Awake()
    {

        playerMask = LayerMask.GetMask("Player");
        floorMask = LayerMask.GetMask("Floor");

        playerAnimator = GetComponentInChildren<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        playerFocus = GetComponent<PlayerFocus>();

        camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        playerDashEvent = new PlayerDashEvent(MoveToward);
        
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse1) )
        {
            // 這邊要改成左鍵優先 左鍵按過後 要放開右鍵再按一次右鍵才會再觸發這裡 1
            playerFocus.Defocus();

            RaycastHit playerHit;
            // 沒射到玩家才做移動或旋轉 避免原地折返
            if (!Physics.Raycast(camRay, out playerHit, camRayLength, playerMask))
            {
                MoveForward();
            }
            else
            {
                isRunning = false;
                currentSpeed = 0;
            }
            Turning();
        }
        else
        {
            isRunning = false;
            currentSpeed = 0;

        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // 攻擊
            
        }

      

    }

    // 移動的概念就是拿getAxis的東西放到rigidbody
    public void MoveForward()
    {
        isRunning = true;
        currentSpeed = Mathf.Lerp(currentSpeed, maxSpeed, acceleration * Time.deltaTime);
        playerRigidbody.MovePosition(transform.position + transform.forward *  currentSpeed * Time.deltaTime);
       
    }

    void Turning()
    {
        camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0.0f;
                
            // 如果游標沒有離玩家太近(不然會原地爆轉)  //現在會折返的原因是 我停止了它旋轉但沒有停止她前進 所以她還是會照原本的路徑前進直到脫離這個範圍又轉頭一次
            if (Mathf.Abs(playerToMouse.x) > 0.05f && Mathf.Abs(playerToMouse.z) > 0.05f)
            {
                // 利用lookeRotation幫忙計算旋轉角度
                Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
                // 旋轉玩家
                
                transform.rotation = Quaternion.Lerp(playerRigidbody.rotation, newRotation, 0.5f);
            }
        }
    }

    public void FaceTo(Vector3 _target, Vector3 _origin, bool noY = true)
    {
        Vector3 targetToOrigin = _target - _origin;
        if (noY == true)
        {
            targetToOrigin.y = 0.0f;
        }

        Quaternion newRotation = Quaternion.LookRotation(targetToOrigin);
      
        transform.rotation = Quaternion.Lerp(playerRigidbody.rotation, newRotation, 0.5f);


    }

    public void MoveToward(Vector3 _targetPos, float _speed, float stopDistance)
    {
        playerAnimator.SetBool("IsDashing", true);
        Vector3 dir = _targetPos - transform.position;

        // 為了處理飛行物體的停止距離
        Vector3 targetPosNoY = _targetPos;
        targetPosNoY.y = 0;
        Vector3 transformpositionNoY = transform.position;
        transformpositionNoY.y = 0;

        if (Vector3.Distance(targetPosNoY, transformpositionNoY) <= stopDistance) // 買東西回來記得把5改成參數船近來 應該會用attackable的radius
        {
            // 如果撞到了stopdistance 把停止的函式加進委派 移除跑向目標的函式movetoward 啟動時則相反
            playerDashEvent += MoveTowardStop;
            playerDashEvent -= new PlayerDashEvent(MoveToward);
            return;
        }

        dir.y = 0;
        dir.Normalize();
        playerRigidbody.MovePosition(transform.position + dir * _speed * Time.deltaTime);
    }

    public void MoveTowardStop(Vector3 _targetPos, float _speed, float stopDistance)
    {
        playerAnimator.SetBool("IsDashing", false);
    }

    void Animating()
    {
        bool walking = Input.GetKey(KeyCode.Mouse1);
        playerAnimator.SetBool("IsRunning", walking);
    }


}
